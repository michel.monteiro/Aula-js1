
/*
//CONDICIONAIS
    //if
        if(condicao){
            //faz uma coisa
        }else { 
            //faz outra coisa
        }



    //if aninhado
        if(condicao1){
            //faz coisa 1
        }else if(condicao2){ 
            //faz coisa 2
        }else if(condicao3){ 
            //faz coisa 3
        }else if(condicao4){ 
            //faz coisa 4
        }else if(condicao5){ 
            //faz coisa 5
        }



    //switch
        switch(condicao){
            case "valor1":
                //faz coisa 1 e possivelmente a coisa 2

        
            case "valor2":
                //faz coisa 2
                break  // ----------->impede que casos seguintes sejam executados
        
            case "valor3":
                //faz coisa 3
                break

            default:
                //faz coisa padrao (eh escolhido quando nenhum dos outros casos ocorre)
                break
        }

*/



console.log(25 + "25")

//true && false = false