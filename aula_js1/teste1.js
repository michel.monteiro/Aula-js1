console.log(var_la_do_final)

{x = 1}
{var y = 2}
{let z = 3}

console.log(x)
console.log(y)
//console.log(z)

console.log("")
console.log("")

{
    let teste_escopo = 5
    {
        //console.log(teste_escopo)     //fora do escopo, vai dar erro
        let teste_escopo = 6
        console.log(teste_escopo)       //printa 6
    }
    console.log(teste_escopo)           //printa 5
}


{
    var variavel_global = 1
    let variavel_local = 2
    const variavel_local_constante = 3
    variavel_global_sem_palavra_chave = 4
}

let variavel_fora_de_um_bloco1
//aqui temos um bloco
{
    let variavel_dentro_de_um_bloco
}
let variavel_fora_de_um_bloco2


var var_la_do_final = 4
console.log(var_la_do_final)



//console.log(variavel)   //fora do escopo, vai dar erro
{
    //console.log(variavel)   //fora do escopo, vai dar erro

    let variavel    //comeco do escopo



    console.log(variavel)
}   //fim do escopo

//console.log(variavel)   //fora do escopo, vai dar erro



var x;          //agora x é do tipo undefined
x = 5           //agora x é do tipo Number
x = "paçoca"    //agora x é do tipo String




console.log(""); console.log(""); console.log(""); console.log("")

console.log(parseInt("25", 6))

parseFloat()


console.log(parseInt("25.7"))       //25
console.log(parseFloat("25.7"))     //25.7


console.log(25 === "25")

array = [25, 30, 35]



array.forEach()



/*  COMENTARIO
    DE
    VARIAS
    LINHAS  */


let controle = 2

switch(controle){
    case 1:


    case 2:


    case 3:

}