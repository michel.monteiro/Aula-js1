//Sintaxe
    //Sintaxe normal
    function nome_da_funcao(parametros) {
        // codigo a ser executado
        return valor_de_retorno //nem toda funcao tem essa expressao
    }

    //Sintaxe arrow function
    nome_da_funcao = (parametros) => {
        // codigo a ser executado
        return valor_de_retorno //nem toda funcao tem essa expressao
    }




//Exemplos
    function soma(x, y){
        return x+y
    }

    incremento = (x)=>{return x+1}




    let num = 3
    console.log(soma(num, 5))   //8

    console.log('')




    for(let i =0; i< 10; i++){
        num = incremento(num)
    }

    console.log(num)    //13