

//  Tipo sao dinamicos no javascript

    let x;          //agora x é do tipo undefined
    x = 5           //agora x é do tipo Number
    x = "paçoca"    //agora x é do tipo String



//  Strings (palavras)
    let carName1 = "Volvo XC60";   // usando aspas duplas
    let carName2 = 'Volvo XC60';   // usando aspas simples

        let answer1 = "It's alright";             // aspas simples dentro de aspas duplas
        let answer2 = "He is called 'Johnny'";    // aspas simples dentro de aspas duplas
        let answer3 = 'He is called "Johnny"';    // aspas duplas dentro de usando aspas simples


//  Numbers (numeros)
    let x1 = 34.00;     // numero com decimais, também conhecido como "float" ou "numero de ponto flutuante"
    let x2 = 34;        // numero sem decimais, também conhecido com "int", "integer" ou "numero inteiro"

    let y = 123e5;      // 12300000    123 x 10^5  =  123 x 100000
    let z = 123e-5;     // 0.00123     123 x 10^-5 =  123 x 0.00000


// Booleans (verdadeiro ou falso)
    {

    let x = 5;
    let y = 5;
    let z = 6;

    let verdadeiro = (x == y)       // Returns true
    let falso = (x == z)            // Returns false

    verdadeiro = true
    falso = false

    }




// Undefined
    let carro;    //variavel sem valor,   valor é undefined, tipo é undefined


    varivavel = 5;
    arivavel = undefined;   //assim resetamos o tipo e valor da variavel pra undefined



// Arrays (tipo composto) (são um tipo especial de Objects)
    const cars = ["Saab", "Volvo", "BMW"];
    const numeros = [1, 2, 3];
    const bagunça = [1, "Saab", true];
 
    bagunça[0]      //1a posicao = 1
    bagunça[1]      //2a posicao = "Saab"
    bagunça[2]      //3a posicao = true



// Objects (tipo composto)   ASSUNTO DA AULA DA TARDE
    const pessoa = {primeiroNome:"John", ultimoNome:"Doe", idade:50, corDoOlho:"blue"};
    //tem propriedades escritas como pares nome:valor

    pessoa.primeiroNome //"John"
    pessoa.ultimoNome   //"Doe"



// Checando valores com typeof
    console.log(typeof bagunça)  //object

