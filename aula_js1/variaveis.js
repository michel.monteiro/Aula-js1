//Regras para nomes de variaveis

    // primeira letra so pode ser uma letra ou o _
    // o resto pode ser letra, _ ou numeros

    var so_letras
    var bagunca123dftasy399293y9299hfhsdxhcs
    //var 1invalida        nao é valida





//Existem 4 maneiras de declarar variaveis em javacript

    var variavel_global1
    //acessivel no programa inteiro (escopo global)
    //pode ser declarada sem valor
    //pode ser usada antes antes de ser declarada
    //pode ser modificada
    //pode ser redefinida


    let variavel_local1      
    //acessivel no interior bloco em que foi declarada (escopo local/de bloco)
    //pode ser declarada sem valor
    //nao pode ser usada antes antes de ser declarada
    //pode ser modificada
    //nao pode ser redefinida no bloco onde foi declarada


    const variavel_local_const1 = 0  
    //acessivel no interior bloco em que foi declarada (escopo local/de bloco)
    //nao pode ser declarada sem valor
    //nao pode ser usada antes antes de ser declarada
    //nao pode ser modificada
    //nao pode ser redefinida no bloco onde foi declarada


    variavel_sem_palavra_chave1 = 0
    //variavel global,  mesmas regras
    






//redeclaracao

    var variavel_global1                //valido
    //let variavel_local1               //invalido, nao pode ser redeclarada
    //const variavel_local_const1 = 0   //invalido, nao pode ser redeclarada
    {
        let variavel_local1             //valido pois esta em um bloco interno  (mesma coisa seria valida para estar do lado de fora do bloco)
        const variavel_local_const1 = 0 //valido pois esta em um bloco interno  (mesma coisa seria valida para estar do lado de fora do bloco)
    }
    variavel_sem_palavra_chave1 = 0








//modificacao

    variavel_global1 = 1                //pode ser modificada
    variavel_local1 = 2                 //pode ser modificada
    //variavel_local_const1 = 3           //nao pode ser modificada, mas o erro so aparece em runtime :)
    variavel_sem_palavra_chave1 = 4     //pode ser modificada









//Blocos
    //dao estrutura ao nosso programa, e permitem outras funcionalidades que veremos mais a frente
    //definidos com "{ }"

    //aqui temos um bloco
    {
        var variavel_global2 = 1
        let variavel_local2 = 2
        const variavel_local_const2 = 3
        variavel_sem_palavra_chave2 = 4

        console.log(variavel_global2)
        console.log(variavel_local2)
        console.log(variavel_local_const2)
        console.log(variavel_sem_palavra_chave2)
    }
    console.log("")

    console.log(variavel_global2)
    //console.log(variavel_local2)     //variavel_local2 nao existe aqui fora do bloco
    //console.log(variavel_local_const2)     //variavel_local_const2 nao existe aqui fora do bloco
    console.log(variavel_sem_palavra_chave2)