
const numero = 27
const string = "33"
const string2 = "33.7"




console.log(numero + string)    //"2733"
console.log(numero + string2)   //"2733.7"

console.log("")






console.log(numero.toString() + string)     //"2733"

console.log("")





console.log(numero + Number(string))    //60
console.log(numero + Number(string2))   //60.7
console.log(String(numero) + string)    //"2733"

console.log("")





console.log(parseInt(string) + numero)      //60
console.log(parseInt(string2) + numero)     //60
console.log(parseFloat(string2) + numero)   //60.7

console.log("")




console.log(parseInt(string, 4))            //15     33 - >  3x4 + 3 = 12 + 3 = 15

console.log("")







console.log(Boolean(25))        //true
console.log(!!25)               //true
console.log(!25)                //false 

console.log("")




console.log(Boolean("string"))        //true
console.log(!!"string")               //true
console.log(!"string")                //false 

console.log("")



console.log(Boolean(0))               //false