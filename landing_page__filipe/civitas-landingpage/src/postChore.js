export const postChores = async (nome, email, senha, data) => {
    console.log(nome);
    try{
        const response = await fetch("http://localhost:3000/usuarios",{
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                nome: `${nome}`,
                email: `${email}`,
                senha: `${senha}`,
                data: `${data}`,
            }),
        });
        const content = await response.json();
        return content;
    }catch (error){
            console.log(error);
        }
    
};
